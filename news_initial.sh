#!/bin/bash

#########################################################################
#   Ubuntu 16.04 NEWS Development environment: Initial config.
#
#   Author: Julio López 
#
#########################################################################

#############
# CONSTANTS #
#############

DEBUG=true
LOG_FILE='/var/log/NEWS_Provision.log'
# Timezone
TIMEZONE="Europe/Madrid" 

# MySQL password
MYSQL_PASS="desarrollo"

function upgradeUbuntu(){
    apt-get -y update
    #Add Ondrej repository for php7
    apt-get -y install python-software-properties software-properties-common
    LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/php
    apt-get -y update
    DEBIAN_FRONTEND=noninteractive apt-get -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" upgrade
}

function initialPackages(){
    # Necessary packages
    apt-get -y install wget vim htop telnet git pkg-config build-essential
    #Apache
    apt-get -y install apache2
    #PHP 7.1
    ####apt-get install php7.0 php7.0-mysql libapache2-mod-php7.0 php7.0-mcrypt php7.0-curl
    apt-get -y install php7.1 php7.1-common php7.1-cli php7.1-common php7.1-mbstring php7.1-pgsql php7.1-dev
    apt-get -y install php7.1-mysql php7.1-xmlrpc php7.1-xml php7.1-gd php7.1-opcache php7.1-imagick

    # Install pngquant package
    apt-get install -y pngquant
}

function installMencached(){
    apt-get install -y memcached php7.1-dev libmemcached-dev 
    cd ~
    git clone https://github.com/php-memcached-dev/php-memcached.git
    cd php-memcached
    git checkout php7
    phpize
    ./configure --disable-memcached-sasl
    make
    make install

    echo "extension=memcached.so" > /etc/php/7.1/mods-available/memcached.ini
    #Enable Memcached
    sudo ln -s /etc/php/7.1/mods-available/memcached.ini /etc/php/7.1/apache2/conf.d/20-memcached.ini
    #Enable Memcached for php-cli
    sudo ln -s /etc/php/7.1/mods-available/memcached.ini /etc/php/7.1/cli/conf.d/20-memcached.ini
}

function timezonedbInstall(){
	pecl install timezonedb
	echo "extension=timezonedb.so" > /etc/php/7.1/mods-available/timezonedb.ini
        #Enable timezonedb
        sudo ln -s /etc/php/7.1/mods-available/timezonedb.ini /etc/php/7.1/apache2/conf.d/20-timezonedb.ini
        #Enable timezonedb for php-cli
        sudo ln -s /etc/php/7.1/mods-available/timezonedb.ini /etc/php/7.1/cli/conf.d/20-timezonedb.ini
}

function moduleRedisInstall(){
	mkdir -p /root/sources/redis
	cd /root/sources/redis
	git clone https://github.com/phpredis/phpredis.git ./
	phpize
	./configure
	make
	make install
	echo "extension=redis.so" > /etc/php/7.1/mods-available/redis.ini
        #Enable timezonedb
        sudo ln -s /etc/php/7.1/mods-available/redis.ini /etc/php/7.1/apache2/conf.d/20-redis.ini
        #Enable timezonedb for php-cli
        sudo ln -s /etc/php/7.1/mods-available/redis.ini /etc/php/7.1/cli/conf.d/20-redis.ini

	rm -Rf /root/sources
}

function installPHPUnit(){
        apt-get -y install phpunit
}

function installMySQL(){
        # Set MySQL root password and install MySQL. Info on unattended install: http://serverfault.com/questions/19367
        echo mysql-server mysql-server/root_password select $MYSQL_PASS | debconf-set-selections
        echo mysql-server mysql-server/root_password_again select $MYSQL_PASS | debconf-set-selections
        apt-get install -y mysql-server mysql-client # install mysql server and client
        service mysql restart # restarting for sanities' sake
}

function setTimeZone(){
    echo $TIMEZONE | sudo tee /etc/timezone
    dpkg-reconfigure --frontend noninteractive tzdata
}
function userConfig(){
    #change root password "echo 'user:password' | chpasswd"
    echo 'root:desarrollo'|chpasswd
    # desa user in apache group
    usermod -a -G desa www-data
    # Apache user in desa group
    usermod -a -G www-data desa
}


#######
## LOG FUNCTIONS
#######
function debug (){
        if [ $DEBUG == true ]; then
                echo "$@"
        fi
}


# Log function, handles input from stdin or from arguments
# Usage: log message in parts 
# Usage2: echo message | log
function log (){
        # If there are parameters read from parameters
        if [ $# -gt 0 ]; then
                echo "[$(date +"%Y-%m-%d %T")] $@" 2>&1 >> $LOG_FILE
                debug "$@"
        else
        # If there are no parameters read from stdin
                while read data; do
                        echo "[$(date +"%Y-%m-%d %T")] $data" 2>&1 >> $LOG_FILE
                        debug $data
                done
        fi
}

#####
# Main Install function
#####
function mainInstall(){
    # Upgrade Ubuntu
    upgradeUbuntu

    # Install initial packages
    initialPackages

    # Mecached Server and PHP Memcached client install
    installMencached

    # PHP Timezonedb install
    timezonedbInstall
    
    # PHP Redis module install
    moduleRedisInstall

    # PHPUnit
    installPHPUnit

    # MySQL install
    installMySQL

    # User Config
    userConfig
    
}

########
# MAIN #
########

echo "***** Provisionando... *****"
export PROVISIONING_NEW_VERSION=1

main_function="mainInstall"
eval ${main_function} | log

unset PROVISIONING_NEW_VERSION

echo "***** Provisión finalizada *****" | log

