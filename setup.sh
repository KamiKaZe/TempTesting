#!/bin/bash

if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

apt-get -y install wget virtualbox

cd 
wget https://releases.hashicorp.com/vagrant/1.9.5/vagrant_1.9.5_x86_64.deb
dpkg -y vagrant_1.9.5_x86_64.deb
rm -f vagrant_1.9.5_x86_64.deb

vagrant plugin install vagrant-vbguest



