TempTesting

Requisitos para Ubuntu;

<pre>
sudo apt-get update
sudo apt-get -y install iptables-persistent
sudo iptables -t nat -A OUTPUT -o lo -p tcp --dport 80 -j REDIRECT --to-port 8080
sudo iptables -t nat -A OUTPUT -o lo -p tcp --dport 443 -j REDIRECT --to-port 8443
sudo bash -c "iptables-save > /etc/iptables/rules.v4" 
</pre>

Descargar setup.sh

<pre>
chmod +x setup.sh
sudo ./setup.sh
</pre>
